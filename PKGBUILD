# Maintainer: Future Linux Team <future_linux@163.com>

pkgname=systemd
pkgver=251
pkgrel=2
pkgdesc="system and service manager"
arch=('x86_64')
url="https://www.freedesktop.org/wiki/Software/systemd/"
license=('GPL2' 'LGPL2.1')
groups=('base')
depends=('acl' 'bash' 'cryptsetup' 'glibc' 'kbd' 'kmod' 'libcap'
    'libelf' 'libgcrypt' 'libidn2' 'libpwquality' 'linux-pam'
    'lz4' 'openssl' 'p11-kit' 'pcre2' 'shadow' 'util-linux' 'xz' 'zstd')
makedepends=('bash-completion' 'git' 'gnu-efi'
    'gperf' 'intltool' 'linux-api-headers'
    'meson' 'p11-kit' 'python-jinja' 'rsync')
optdepends=('libmicrohttpd: remote journald capabilities'
    'quota-tools: kernel-level quota management'
    'polkit: allow administration as unprivileged user'
    'curl: machinectl pull-tar and pull-raw'
    'libfido2: unlocking LUKS2 volumes with FIDO2 token'
    'tpm2-tss: unlocking LUKS2 volumes with TPM2')
backup=(etc/pam.d/systemd-user
    etc/systemd/coredump.conf
    etc/systemd/journal-upload.conf
    etc/systemd/journald.conf
    etc/systemd/logind.conf
    etc/systemd/networkd.conf
    etc/systemd/oomd.conf
    etc/systemd/pstore.conf
    etc/systemd/resolved.conf
    etc/systemd/sleep.conf
    etc/systemd/system.conf
    etc/systemd/timesyncd.conf
    etc/systemd/user.conf
    etc/udev/udev.conf)
install=${pkgname}.install
source=(https://github.com/systemd/systemd/archive/v${pkgver}/${pkgname}-${pkgver}.tar.gz
    systemd-binfmt.hook
    systemd-catalog.hook
    systemd-daemon-reload.hook
    systemd-hook
    systemd-hwdb.hook
    systemd-sysctl.hook
    systemd-sysusers.hook
    systemd-tmpfiles.hook
    systemd-udev-reload.hook
    systemd-update.hook
    systemd-user)
sha256sums=(0ecc8bb28d3062c8e58a64699a9b16534554bb6a01efb8d5507c893db39f8d51
    a938701f4244ebdc4ae88ed0032a8315b68f086c6e45d9f2e34ef9442a8f6b47
    be3c749ed51ee26c8412e0304a41ac5e00e668f4d5227b5f359a3b0c31d18d5d
    e112fab7ff7134df71580617d03c59ce0cd74540facece6fa40ba05e4644e9bd
    46bc9d0259b503e97bdf75aa23e8277aef7813fd7bc76b9daac1741d1d30f3d4
    b19b23467dc33b3e8267cabea10726b0d181e0de1b491ec538b9fb620bccf57f
    1af0fbaeaf974fe3d8409854179fac68e8461524dd49849b7e889374363ce3c9
    f55162dc55268aa3850ef410aa1e444defa0f68b996e415c8b8bc04d63053e7a
    3cfdc3c21d32cc35b1315f4ff4df87238bc7d2c27bdcf4e5a70600832c481e95
    f6364443609b1d5a07f385e7228ace0eae5040ae3bbd4e00ed5033ef1b19e4b9
    1090b7b1edba2042298b609a77bbe122982ca936208408fb79d77b33a2f3c27a
    85600fbd0a8568077b77749375cba6e2bd2df695b3c9bb11636c46d2629ad172)

prepare() {
    cd ${pkgname}-${pkgver}

    sed -e 's/GROUP="render"/GROUP="video"/' \
        -e 's/GROUP="sgx", //'               \
        -i  rules.d/50-udev-default.rules.in
}

build() {
    cd ${pkgname}-${pkgver}

    meson --prefix=/usr                         \
        --buildtype=release                     \
        -Ddefault-dnssec=no                     \
        -Dfirstboot=false                       \
        -Dinstall-tests=false                   \
        -Dldconfig=false                        \
        -Dsysusers=true                         \
        -Drpmmacrosdir=no                       \
        -Dhomed=true                            \
        -Duserdb=true                           \
        -Dman=false                             \
        -Dmode=release                          \
        -Dpamconfdir=no                         \
        -Dpamconfdir=/etc/pam.d                 \
        -Dlibidn2=true                          \
        -Dlz4=true                              \
        -Dlibidn2=true                          \
        -Dgnu-efi=true                          \
        -Drpmmacrosdir=no                       \
        -Dfallback-hostname='futurelinux'       \
        -Dlocalegen-path=/usr/sbin/locale-gen   \
        -Dnologin-path=/usr/sbin/nologin        \
        -Dsbat-distro='future'                  \
        -Dsbat-distro-summary='Future Linux'    \
        -Dsbat-distro-pkgname=${pkgname}        \
        -Dsbat-distro-version=${pkgver}         \
        -Dsbat-distro-url="https://gitlab.com/future-linux/packages/Core/${pkgname}/"  \
        build

    meson compile -C build
}

package() {
    cd ${pkgname}-${pkgver}

    DESTDIR=${pkgdir} meson install -C build

    rm ${pkgdir}/usr/share/factory/etc/{issue,nsswitch.conf}
    sed -e '/^C \/etc\/nsswitch\.conf/d' \
        -e '/^C \/etc\/issue/d'          \
        -i  ${pkgdir}/usr/lib/tmpfiles.d/etc.conf

    echo 'disable *' >${pkgdir}/usr/lib/systemd/system-preset/99-default.preset

    install -d -o root -g root -m 2755 ${pkgdir}/var/log/journal

    install -d -o root -g 102 -m 0750 ${pkgdir}/usr/share/polkit-1/rules.d

    install -Dm644 ${srcdir}/systemd-user ${pkgdir}/etc/pam.d/systemd-user

    # pacman hooks
    install -Dm755 ${srcdir}/systemd-hook ${pkgdir}/usr/share/libalpm/scripts/systemd-hook
    install -Dm644 -t ${pkgdir}/usr/share/libalpm/hooks ${srcdir}/*.hook
}
